const Winston = require("../config/winston");
const Joi = require("joi");
const Context = require("../Context");
const jwt = require("jsonwebtoken");

// this returns controller has : { handler, schema, auth/security }
module.exports = (controller) => async (req, res) => {
  req.context = new Context();

  //create meta
  let meta = {
    method: req.method,
    url: req.originalUrl,
    header: req.headers,
    body: req.body,
    start_time: new Date(),
    code: 200,
    message: "Success",
  };

  //******TRY*******
  try {
    // error pop up method
    req.throw = (code, message) => {
      let error = new Error(message);
      error.code = code;
      throw error;
    };

    // check for validation
    if (controller.schema) {
      const validation = await Joi.compile(controller.schema)
        .prefs({ errors: { label: "key" } })
        .validate(req.body);
      if (validation.error) {
        let message = validation.error.details
          .map((details) => details.message)
          .join(", ");
        req.throw(400, message);
      }
    }

    // check for auth
    if (controller.auth) {
      const authHeader = req.headers["authorization"];
      const token = authHeader && authHeader.split(" ")[1];
      console.log(token);
      // So we know that they have not send a token to us (un authorized)
      if (token == null) return res.status(401).send("No token set!");

      // so that we have valid token we have to verify token
      jwt.verify(token, process.env.jwt_private_key, (err, user) => {
        // 403: we see that you have a token but the token is not valid!
        if (err) return res.sendStatus(403);

        // so now we know that the token is valid
        // set user in req
        req.user = user;
      });
    }

    // // user
    // req.getUser = async () => {
    //   if (controller.auth)
    //     if (!req.user)
    //       req.user = await req.context.getUser(req.session.user_id);
    //   return req.user;
    // };

    // call controller
    if (controller.handler) result = await controller.handler(req, res);
    if (!result) result = "Failed";
    //*******CATCH*******
  } catch (error) {
    if (error.code === undefined) {
      error.code = 500;
    }
    meta.code = error.code;
    meta.message = error.message;
    meta.error = error;
    if (error.code) result = meta.message;
    else {
      result = "Sorry, internl server error.";
    }
  }

  meta.end_time = new Date();
  meta.duration = meta.end_time - meta.start_time;

  // add winston info
  Winston.info(meta);
  return res.status(meta.code).send(result);
};
