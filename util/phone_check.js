const Phone = require("phone");

async function isValid(number) {
  return Phone.phone(number).isValid;
}

module.exports = isValid;
