const router = require("express").Router();

const requestHandler = require("../middleware/requestHandler");

const getAllController = require("../controller/post/getAll");
const getAllUserPostsController = require("../controller/post/getAllUserPosts");
const getController = require("../controller/post/get");
const addController = require("../controller/post/add");
const deleteController = require("../controller/post/delete");
const updateController = require("../controller/post/update");

router.get("/", requestHandler(getAllController));
router.get("/user-posts", requestHandler(getAllUserPostsController));
router.post("/", requestHandler(addController));
router.get("/:id", requestHandler(getController));
router.put("/:id", requestHandler(updateController));
router.delete("/:id", requestHandler(deleteController));

module.exports = router;
