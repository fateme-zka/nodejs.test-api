const router = require("express").Router();

const requestHandler = require("../middleware/requestHandler");

const getAllController = require("../controller/category/getAll");
const getController = require("../controller/category/get");
const addController = require("../controller/category/add");
const updateController = require("../controller/category/update");
const deleteController = require("../controller/category/delete");

router.get("/", requestHandler(getAllController));
router.post("/", requestHandler(addController));
router.get("/:id", requestHandler(getController));
router.put("/:id", requestHandler(updateController));
router.delete("/:id", requestHandler(deleteController));

module.exports = router;
