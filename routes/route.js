const router = require("express").Router();

router.use("/users", require("./user"));
router.use("/posts", require("./post"));
router.use("/categories", require("./category"));

module.exports = router;
