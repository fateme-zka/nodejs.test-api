const router = require("express").Router();

const requestHandler = require("../middleware/requestHandler");

const getAllController = require("../controller/user/getAll");
const getController = require("../controller/user/get");
const registerController = require("../controller/user/register");
const loginController = require("../controller/user/login");
const logoutController = require("../controller/user/logout");

router.get("/", requestHandler(getAllController));
router.post("/register", requestHandler(registerController));
router.post("/login", requestHandler(loginController));
router.get("/logout", requestHandler(logoutController));
router.get("/:id", requestHandler(getController));

module.exports = router;
