const Joi = require("joi");

// empty schema
const schema = Joi.object({});

const handler = async (req, res) => {
  return req.context.getAllPosts(req, res);
};

module.exports = { handler, schema, auth: false };
