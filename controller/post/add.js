const Joi = require("joi");

const schema = Joi.object({
  title: Joi.string().required(),
  content: Joi.string().required(),
  summary: Joi.string().required(),
  image: Joi.string(),
  author_id: Joi.number().required(),
  category_id: Joi.number().required(),
});

const handler = async (req, res) => {
  return req.context.createPost(req, res);
};

module.exports = { handler, schema, auth: true };
