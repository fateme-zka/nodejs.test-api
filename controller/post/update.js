const Joi = require("joi");
const Context = require("../../Context");

const schema = Joi.object({
  title: Joi.string(),
  content: Joi.string(),
  summary: Joi.string(),
  image: Joi.string(),
  author_id: Joi.number(),
  category_id: Joi.number(),
});

const handler = async (req, res) => {
  return req.context.updatePost(req, res);
};

module.exports = { handler, schema, auth: true };
