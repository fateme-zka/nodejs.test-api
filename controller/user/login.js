const Joi = require("joi");

const schema = Joi.object({
  username: Joi.string().required(),
  email: Joi.string().required().email(),
  password: Joi.string().min(6).alphanum().required(),
});

const handler = async (req, res) => {
  return req.context.loginUserByEmail(req, res);
};

module.exports = { handler, schema, auth: false };
