const Joi = require("joi");

const schema = Joi.object({});

const handler = async (req, res) => {
  return req.context.logoutUser(req, res);
};

module.exports = { handler, schema, auth: true };
