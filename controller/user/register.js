const Joi = require("joi");

const schema = Joi.object({
  username: Joi.string().required(),
  name: Joi.string(),
  family: Joi.string(),
  email: Joi.string().required().email(),
  phone: Joi.string(),
  password: Joi.string().min(6).alphanum().required(),
});

const handler = async (req, res) => {
  return req.context.registerUser(req, res);
};

module.exports = { handler, schema, auth: false };
