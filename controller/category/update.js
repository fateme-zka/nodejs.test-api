const Joi = require("joi");

const schema = Joi.object({
  name: Joi.string().required(),
});

const handler = async (req, res) => {
  return req.context.updateCategory(req, res);
};

module.exports = { handler, schema, security: true };
