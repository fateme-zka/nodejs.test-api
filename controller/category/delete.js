const Joi = require("joi");

// empty schema
const schema = Joi.object({});

const handler = async (req, res) => {
  return req.context.deleteCategory(req, res);
};

module.exports = { handler, schema, auth: true };
