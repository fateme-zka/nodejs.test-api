const winston = require("winston");

let logger = new winston.createLogger({
  format: winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.prettyPrint()
  ),
  transports: [
    new winston.transports.Console({
      level: "info",
      colorize: true,
      // timestamp: true,
      // json: true,
      handleExceptions: true,
    }),
    new winston.transports.Console({
      level: "error",
      colorize: true,
      // timestamp: true,
      // json: true,
      handleExceptions: true,
    }),
    new winston.transports.File({
      filename: "log/infoLogs.log",
      level: "info",
      colorize: true,
      timestamp: true,
      json: true,
      handleExceptions: true,
    }),
    new winston.transports.File({
      filename: "log/errorLogs.log",
      level: "error",
      colorize: true,
      timestamp: true,
      json: true,
      handleExceptions: true,
    }),
  ],
  exitOnError: false,
});

logger.stream = {
  write: function (message, encoding) {
    logger.info(message);
  },
};

module.exports = logger;
