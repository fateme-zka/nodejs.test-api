// Dotenv
require("dotenv").config();

// Winston
const Winston = require("./config/winston");

// Express
const express = require("express");
const app = express();

// json middleware
app.use(express.json());

// Database
const Context = require("./Context");
const context = new Context();
context.init();

// Api routes
app.use("/", require("./routes/route"));

// Start server
const port = process.env.server_port;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
  Winston.info(`Server listening on port ${port}`);
});
