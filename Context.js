const Sequelize = require("sequelize");
const Phone = require("phone");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const calculate_reading_time = require("./util/calculate_reading_time");

module.exports = class Context {
  constructor() {
    this.database = require("./config/database");
  }

  init() {
    // Insert Models
    const User = require("./model/User");
    const Post = require("./model/Post");
    const Category = require("./model/Category");

    // Create Tables (base on models)
    const user = this.addFunction(User(this.database, Sequelize.DataTypes));
    const post = this.addFunction(Post(this.database, Sequelize.DataTypes));
    const category = this.addFunction(
      Category(this.database, Sequelize.DataTypes)
    );

    //Set ForeignKeys
    post.belongsTo(user, {
      foreignKey: { name: "author_id", allowNull: false },
    });
    post.belongsTo(category, {
      foreignKey: { name: "category_id", allowNull: false },
    });

    // this.getColumns(user);

    // calling (sync) method means create this database:
    this.database.sync({ force: false });
  }

  addFunction(model) {
    model.getColumns = (secure) => {
      return this.getColumns(model, secure);
    };
    model.secure = (obj) => {
      let Cs = this.getColumns(model, true);
      for (let i = 0; i < Cs.length; i++) {
        const element = Cs[i];
        delete obj.dataValues[element];
      }
      return obj;
    };
    return model;
  }

  getColumns(model, secure) {
    let ans = [];
    for (const [key, value] of Object.entries(model.fieldRawAttributesMap)) {
      if (secure === true) {
        if (value.secure) ans.push(key);
      } else if (secure === false) {
        if (!value.secure) ans.push(key);
      } else ans.push(key);
    }
    return ans;
  }

  //#region user-------------------------------------------------
  async getAllUsers(req, res) {
    return await this.database.models.user
      .findAll()
      .then((users) => res.send(users))
      .catch((err) => res.status(500).send(err.message));
  }

  async getUserById(req, res) {
    return await this.database.models.user
      .findOne({ where: { id: req.params.id } })
      // to handle the result
      .then((data) => {
        if (data) res.json(data);
        else res.status(400).send("There is no user with this id!");
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async registerUser(req, res) {
    let { username, name, family, email, phone } = req.body;

    // check email
    await this.database.models.user
      .findOne({ where: { email } })
      .then((user) => {
        if (user) res.status(400).send("Email is already exists.");
      })
      .catch((err) => res.status(500).send(err.message));

    // check phone
    if (phone) {
      if (!(await Phone.phone(phone).isValid))
        return res.status(400).send("Invalid phone number.");
      let user = await this.database.models.user.findOne({ where: { phone } });
      if (user) return res.status(400).send("Phone is already exists.");
    }

    // hash password
    const salt = parseInt(process.env.bcrypt_salt);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    console.log(hashedPassword);

    // create user in db
    return await this.database.models.user
      .create({
        username: username,
        name: name,
        family: family,
        email: email,
        phone: phone,
        password: hashedPassword,
      })
      .then((user) => {
        console.log(user);
        res.send(`User ${user.name} created successfully.`);
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async loginUserByEmail(req, res) {
    let { username, email, password } = req.body;
    let user = await this.database.models.user.findOne({
      where: { username: username, email: email },
    });

    const payload = {
      id: user.id,
      username: username,
      name: user.name,
      family: user.family,
      email: user.email,
      phone: user.phone,
      password: user.password,
      is_admin: user.is_admin,
    };

    if (user) {
      // if everything was ok
      if (await bcrypt.compare(password, user.password)) {
        // pass (payload, secretKey)
        const accessToken = jwt.sign(payload, process.env.jwt_private_key);
        req.headers["authorization"] = "Bearer " + accessToken;
        res.send({ accessToken: accessToken });
      } else {
        res.status(400).send("Password is not correct!");
      }
    } else {
      res.status(400).send("User with this email or username is not exist.");
    }
  }

  async logoutUser(req, res) {
    const user = req.user;
    req.headers["authorization"] = null;
    res.send(`${user.name} is logged out.`);
  }

  //#endregion

  //#region post-------------------------------------------------
  async getAllPosts(req, res) {
    const category_name = req.query.category;
    if (category_name) {
      // get the category
      const category = await this.database.models.category.findOne({
        where: { name: category_name },
      });
      // return all posts in that category
      return await this.database.models.post
        .findAll({ where: { category_id: category.id } })
        .then((posts) => res.send(posts))
        .catch((err) => res.status(500).send(err.message));
    }
    // otherwise return all posts
    return await this.database.models.post
      .findAll()
      .then((posts) => res.send(posts))
      .catch((err) => res.status(500).send(err.message));
  }

  async getAllUserPosts(req, res) {
    const user_id = req.user.id;
    return await this.database.models.post
      .findAll({ where: { author_id: user_id } })
      .then((posts) => res.send(posts))
      .catch((err) => res.status(500).send(err.message));
  }

  async getPostById(req, res) {
    return await this.database.models.post
      .findOne({ where: { id: req.params.id } })
      // to handle the result
      .then((data) => {
        if (data) res.json(data);
        else res.status(400).send("There is no post with this id!");
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async createPost(req, res) {
    let { title, content, summary, image, category_id } = req.body;
    let reading_time = calculate_reading_time(content);
    let current_author_id = req.user.id;
    // console.log(req.user);
    // return res.send(`User ID: ${req.user.id}`);

    return await this.database.models.post
      .create({
        title,
        reading_time,
        content,
        summary,
        image,
        current_author_id,
        category_id,
      })
      .then((post) => {
        console.log(post);
        res.send(`Post ${post.title} created successfully.`);
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async updatePost(req, res) {
    const post_id = req.params.id;
    let { title, content, summary, image, category_id } = req.body;
    let reading_time = calculate_reading_time(content);
    let current_author_id = req.user.id;
    let post = await this.database.models.post.findOne({
      where: { id: post_id },
    });
    if (post.author_id !== current_author_id) {
      return res.status(400).send("Current user is not author of this post!");
    }
    // return res.send("USER ID:" + current_author_id);

    return await this.database.models.post
      .update(
        {
          title: title,
          reading_time: reading_time,
          content: content,
          summary: summary,
          image: image,
          category_id: category_id,
        },
        { where: { id: post_id } }
      )
      .then((post) => {
        console.log(post);
        res.send(`${post}:updated successfully.`);
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async deletePost(req, res) {
    let post_id = req.params.id;

    return await this.database.models.post
      .destroy({ where: { id: post_id } })
      .then((data) => {
        if (data) res.send(`${data}:post deleted.`);
        else res.send("Post with this id does not exist.");
      })
      .catch((err) => res.status(500).send(err.message));
  }

  // async getPostsOfCategory(req, res) {
  //   return await this.database.models.post.findAll({
  //     where: { category_id: },
  //   });
  // }

  //#endregion

  //#region category----------------------------------------------
  async getAllCategories(req, res) {
    return await this.database.models.category
      .findAll()
      .then((categories) => res.send(categories))
      .catch((err) => res.status(500).send(err.message));
  }

  async getCategoryById(req, res) {
    return await this.database.models.category
      .findOne({ where: { id: req.params.id } })
      // to handle the result
      .then((category) => {
        if (category) res.json(category);
        else res.status(400).send("There is no category with this id!");
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async createCategory(req, res) {
    let { name } = req.body;

    // check name
    let category = await this.database.models.category.findOne({
      where: { name },
    });

    if (category) return res.status(400).send("Category is already exists.");

    return await this.database.models.category
      .create({ name })
      .then((category) => {
        console.log(category);
        res.send(`Category ${category.name} created successfully.`);
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async updateCategory(req, res) {
    const category_id = req.params.id;
    let { name } = req.body;

    // // check name
    // category = await context.database.models.category.findOne({
    //   where: { name },
    // });
    // if (category)
    //   return res.status(400).send("This Category name is already exists.");

    return await this.database.models.category
      .update({ name: name }, { where: { id: category_id } })
      .then((category) => {
        console.log(category);
        res.send(`${category}: category updated successfully.`);
      })
      .catch((err) => res.status(500).send(err.message));
  }

  async deleteCategory(req, res) {
    let category_id = req.params.id;

    return await this.database.models.category
      .destroy({ where: { id: category_id } })
      .then((data) => {
        if (data) res.send(`${data}:category deleted!`);
        else res.send("Category with this id does not exist.");
      })
      .catch((err) => res.status(500).send(err.message));
  }
  //#endregion
};
